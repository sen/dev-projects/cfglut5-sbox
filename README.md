# TODO add comments

# To simulate

You will need Xilinx UNISIMS library.

Either:

- you have Xilinx toolchain installed and you know how to build and use the provided libraries
- or, you can use the open source version form Xilinx Github

For the second case,

```bash
git submodule update --init
```

should do the trick.
