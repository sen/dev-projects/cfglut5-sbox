module sbox4(
  input[3:0] i,
  output[3:0] o,
  input cfg_clk,
  input cfg_en,
  input [3:0] cfg_i
);

parameter [63:0]INIT = 64'h2174_8FE3_DA09_B65C;

// used to initialize the LUTs
function [3:0][15:0] init_lut(input[63:0]T);
  for(int n=0;n<4;n++)
    for(int b=0; b<16;b++)
      init_lut[n][b] = T[4*b+n];
endfunction

localparam [3:0][15:0]INIT_LUT =init_lut(INIT);

genvar n;
generate
for(n=0;n<4;n++)
begin:B
CFGLUT5 #(.INIT({16'h0000,INIT_LUT[n]}))
   lut4 (
     .I0  ( i[0]     ) ,
     .I1  ( i[1]     ) ,
     .I2  ( i[2]     ) ,
     .I3  ( i[3]     ) ,
     .I4  ( 1'b0     ) ,
     .O5  ( o[n]     ) ,
     .O6  ( /*nc*/   ) ,
     .CLK ( cfg_clk  ) ,
     .CE  ( cfg_en   ) ,
     .CDI ( cfg_i[n] ) ,
     .CDO ( /*nc*/   )
   );
end
endgenerate

endmodule
