module sbox32(
  input [31:0] i,
  output[31:0] o,
  input cfg_clk,
  input cfg_en,
  input [31:0] cfg_i
);

parameter [63:0]INIT[0:7]='{
  64'h0000_0000_0000_0001,
  64'h0000_0000_0000_0010,
  64'h0000_0000_0000_0100,
  64'h0000_0000_0000_1000,
  64'h0000_0000_0001_0000,
  64'h0000_0000_0010_0000,
  64'h0000_0000_0100_0000,
  64'h0000_0000_1000_0000
};

genvar n;
generate
for(n=0;n<8;n++)
begin:nib
sbox4 #(.INIT(INIT[n]))
   sbox (
     .i       ( i[3+4*n -:4]     ) ,
     .o       ( o[3+4*n -:4]     ) ,
     .cfg_i   ( cfg_i[3+4*n -:4] ) ,
     .cfg_clk ( cfg_clk          ) ,
     .cfg_en  ( cfg_en           )
   );
end
endgenerate
endmodule
