.PHONY: compile sim clean

compile:
	vlib work
	vlog +acc *.sv
	vlog +acc XilinxUnisimLibrary/verilog/src/unisims/CFGLUT5.v

sim:
	vsim -c sbox64_tb -do "run -all"

clean:
	rm -fr work transcript vsim.wlf
