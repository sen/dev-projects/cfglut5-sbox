`timescale 1ns/100ps
module sbox64_tb();

bit [15:0][3:0] i;
wire[15:0][3:0] o;
bit cfg_clk;
bit cfg_en_l;
bit cfg_en_h;
bit [31:0] cfg_i;

sbox64 dut(.*);

logic [15:0][3:0] cfg [16] ='{
  64'h2174_8FE3_DA09_B65C,
  64'h0000_0000_0000_0001,
  64'h0000_0000_0000_0010,
  64'h0000_0000_0000_0100,
  64'h0000_0000_0000_1000,
  64'h0000_0000_0001_0000,
  64'h0000_0000_0010_0000,
  64'h0000_0000_0100_0000,
  64'h0000_0000_1000_0000,
  64'h0000_0001_0000_0000,
  64'h0000_0010_0000_0000,
  64'h0000_0100_0000_0000,
  64'h0000_1000_0000_0000,
  64'h0001_0000_0000_0000,
  64'h0010_0000_0000_0000,
  64'h0100_0000_0000_0000
};

bit[3:0] v;

initial
begin
    // on test à vide pour vérifier l'initialisation par les paramètres
    $display("--- TEST: initialisation par les parametres ----");
    $display("sbox ---------------------------------");
    for(int n=0;n<16;n++)
    begin
      v = n;
      i = {16{v}};
      #10ns;
      $display("i=%1h  : %1h %1h %1h %1h %1h %1h %1h %1h %1h %1h %1h %1h %1h %1h %1h %1h",
                n,
                o[15], o[14], o[13], o[12], o[11], o[10], o[9], o[8],
                o[7], o[6], o[5], o[4], o[3], o[2], o[1], o[0]
                );
      #10ns;
    end
    $display("--------------------------------------");

  // deux tests de re-configuration
  for(int test=0; test<2; test++)
  begin
    #200ns;
    // configuration des 8 première sboxes
    // la config fait 32 bits
    // le premier bit qui rentre se retrouve dans le poids fort
    cfg_en_l = 1;
    // nible par nible (4 bits à chaque fois)
    // en parallèle pour les 8 sboxes
    for(int ni=15; ni>=0; ni--)
    begin
      cfg_i = { cfg[7][ni], cfg[6][ni], cfg[5][ni], cfg[4][ni], cfg[3][ni], cfg[2][ni], cfg[1][ni], cfg[0][ni] };
      cfg_clk = 1;
      #10ns;
      cfg_clk = 0;
      #10ns;
    end
    cfg_en_l = 0;
    #100ns;
    // configuration des 8 dernières sboxes
    cfg_en_h = 1;
    for(int ni=15; ni>=0; ni--)
    begin
      cfg_i = { cfg[15][ni], cfg[14][ni], cfg[13][ni], cfg[12][ni], cfg[11][ni], cfg[10][ni], cfg[9][ni], cfg[8][ni] };
      cfg_clk = 1;
      #10ns;
      cfg_clk = 0;
      #10ns;
    end
    cfg_en_h = 0;
    #100ns;
    // on test
    $display("--- TEST: config %2d ----", test);
    $display("sbox ---------------------------------");
    for(int n=0;n<16;n++)
    begin
      v = n;
      i = {16{v}};
      #10ns;
      $display("i=%1h  : %1h %1h %1h %1h %1h %1h %1h %1h %1h %1h %1h %1h %1h %1h %1h %1h",
                n,
                o[15], o[14], o[13], o[12], o[11], o[10], o[9], o[8],
                o[7], o[6], o[5], o[4], o[3], o[2], o[1], o[0]
                );
      #10ns;
    end
    $display("--------------------------------------");
    // pour le second test, on change les tables
    if(test==0)
      for(int tab=0;tab<16;tab++)
        cfg[tab] = ~cfg[tab];
  end
  $finish();
end

endmodule
